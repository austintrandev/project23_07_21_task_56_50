package com.task56_50;

import java.util.ArrayList;

public class Country {
	private String CountryCode;
	private String CountryName;
	ArrayList<Region> regions = new ArrayList();
	ArrayList<Country> country = new ArrayList();

	public Country() {

	}

	public Country(String CountryCode, String CountryName) {
		this.CountryCode = CountryCode;
		this.CountryName = CountryName;
	}

	/**
	 * @return the regions
	 */
	public ArrayList<Region> getRegions() {
		return regions;
	}

	/**
	 * @param regions the regions to set
	 */
	public void setRegions(ArrayList<Region> regions) {
		this.regions = regions;
	}

	public void addRegion(Region newRegion) {
		this.regions.add(newRegion);
	}

	public void removeRegion(int regionIndex) {
		this.regions.remove(regionIndex);
	}

	public void addCountry(Country country) {
		this.country.add(country);
	}

	public void printCountry() {
		
			System.out.println(this.country);
		
	}
	@Override
	public String toString() {
		String Result = "";
		if (this.CountryCode == "VN") {
			Result += "Country Name is: " + this.CountryName + "\n " + "Region List of this country: " + this.regions;
		}
		else {
			Result += "\n" + "Country Name is: " + this.CountryName;
		}
		return Result;
	}
}
