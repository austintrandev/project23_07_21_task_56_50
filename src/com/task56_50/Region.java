package com.task56_50;

import java.util.ArrayList;

public class Region extends Country {
	protected String regionCode;
	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	protected String regionName;

	public Region(String regionCode, String regionName) {
		super(regionCode, regionName);
		this.regionCode = regionCode;
		this.regionName = regionName;
	}
	
	@Override
	public String toString() {
		return "Region [RegionCode=" + regionCode + ", RegionName=" + regionName + "]" + "\n";
	}
}
