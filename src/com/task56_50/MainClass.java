package com.task56_50;

public class MainClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Region hcm = new Region("HCM", "Ho Chi Minh");
		Region hanoi = new Region("HN", "Ha noi");
		Region danang = new Region("DN", "Da Nang");
		Country vietnam = new Country("VN", "Viet Nam");
		vietnam.addRegion(hcm);
		vietnam.addRegion(hanoi);
		vietnam.addRegion(danang);
		Region newyork = new Region("NY", "New York");
		Region hawaii = new Region("HW", "Hawaii");
		Country usa = new Country("USA", "American");
		usa.addRegion(newyork);
		usa.addRegion(hawaii);
		Country japan = new Country("JP", "Japan");
		Region tokyo = new Region("TK", "Tokyo");
		japan.addRegion(tokyo);
		Country korea = new Country("KR", "Korea");
		Region seoul = new Region("SO", "Seoul");
		korea.addRegion(seoul);
		Country worldwide = new Country();
		worldwide.addCountry(vietnam);
		worldwide.addCountry(usa);
		worldwide.addCountry(japan);
		worldwide.addCountry(korea);
		worldwide.printCountry();
		
	}

}
